//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Md. Ebrahim Joy on 3/7/23.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
